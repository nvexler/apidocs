# Code Lookups for Groups

```
GET /v2/codes/groups.{format}
```

## Description

> This method returns a list of all code lookups for groups (faculty).

## Summary

<table>
  <tr>
    <td><b>Name</b></td>
    <td><b>Value</b></td>
    <td><b><b>Name</b></b></td>
    <td><b>Value</b></td>
  </tr>
  <tr>
    <td><b>Request Protocol</b></td>
    <td>GET</td>
    <td><b>Requires API Key</b></td>
    <td>Yes</td>
  </tr>
  <tr>
    <td><b>Method ID</b></td>
    <td>1229</td>
    <td><b>Enabled</b></td>
    <td>Yes</td>
  </tr>
  <tr>
    <td><b>Service Name</b></td>
    <td>codes</td>
    <td><b>Service ID</b></td>
    <td>263</td>
  </tr>
  <tr>
    <td><b>Information Steward</b></td>
    <td>Institution of Analysis & Planning (IAP)</td>
    <td><b>Data Type</b></td>
    <td>CSV</td>
  </tr>
  <tr>
    <td><b>Update Frequency</b></td>
    <td>When updated by steward/via pull request</td>
    <td><b>Cache Time</b></td>
    <td>0 seconds</td>
  </tr>
</table>


### Notes

- Usage won't increase if there is no data returned
- This data is community curated on github
- In Quest, there are units and groups. Units are inside groups. A unit is like a department and a group is like a faculty. Reason: not all units and departments and not all groups are faculties.
- Any value can be `null`


### Sources

- https://github.com/uWaterloo/Datasets/blob/master/Quest/AcademicGroups.csv


## Parameters

```
GET /v2/codes/groups.{format}
```

<table>
  <tr>
    <td><b>Parameter</b></td>
    <td><b>Type</b></td>
    <td><b><b>Required</b></b></td>
    <td><b>Description</b></td>
  </tr>
  <tr>
    <td><b>key</b></td>
    <td>filter</td>
    <td><i>yes</i></td>
    <td>Your API key</td>
  </tr>
  <tr>
    <td><b>callback</b></td>
    <td>filter</td>
    <td><i>no</i></td>
    <td>JSONP callback format</td>
  </tr>
</table>

**Output Formats**

- json
- xml


## Examples

```
GET /v2/codes/groups.{format}
```

- **https://api.uwaterloo.ca/v2/codes/groups.json**
- **https://api.uwaterloo.ca/v2/codes/groups.xml**
- **https://api.uwaterloo.ca/v2/codes/groups.json?callback=myResponse**


## Response

<table>
  <tr>
    <td><b>Field Name</b></td>
    <td><b>Type</b></td>
    <td><b>Value Description</b></td>
  </tr>
  <tr>
    <td><b>group</b></td>
    <td>string</td>
    <td>Group</td>
  </tr>
  <tr>
    <td><b>description</b></td>
    <td>string</td>
    <td>Description of group</td>
  </tr>
</table>


Any value can be `null`

## Output

#### JSON

```json
{
  "meta":{
    "requests":111,
    "timestamp":1381952872,
    "status":200,
    "message":"Request successful",
    "method_id":1229,
    "version":2.07,
    "method":{
      
    }
  },
  "data":[
    {
      "group":"AHS",
      "description":"Applied Health Sciences"
    },
    {
      "group":"ART",
      "description":"Arts"
    },
    {
      "group":"CGC",
      "description":"Conrad Grebel College"
    },
    {
      "group":"ENG",
      "description":"Engineering"
    },
    {
      "group":"ENV",
      "description":"Environment"
    },
    {
      "group":"GRAD",
      "description":"Graduate Studies"
    },
    {
      "group":"IS",
      "description":"Independent Studies"
    },
    {
      "group":"MAT",
      "description":"Mathematics"
    },
    {
      "group":"REN",
      "description":"Renison University College"
    },
    {
      "group":"SCI",
      "description":"Science"
    },
    {
      "group":"STJ",
      "description":"St Jerome's University"
    },
    {
      "group":"STP",
      "description":"St Paul's University College"
    },
    {
      "group":"THL",
      "description":"Theology"
    },
    {
      "group":"VPA",
      "description":"Interdisciplinary Studies"
    }
  ]
}
```

